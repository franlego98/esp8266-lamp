set(COMPONENT_SRCS "main.c" 
				   "wifi.c"
				   "leds.c"
				   "networking.c") 

set(COMPONENT_ADD_INCLUDEDIRS ".")

register_component()
