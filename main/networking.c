#include "networking.h"
#include "leds.h"

extern fotogram pixels_buff;

void server_task(void *pvParameters){
    char rx_buffer[128];
    char addr_str[128];
    int addr_family;
    int ip_protocol;

	struct sockaddr_in destAddr;
	destAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	destAddr.sin_family = AF_INET;
	destAddr.sin_port = htons(PORT);
	addr_family = AF_INET;
	ip_protocol = IPPROTO_IP;
	inet_ntoa_r(destAddr.sin_addr, addr_str, sizeof(addr_str) - 1);

	int listen_sock = socket(addr_family, SOCK_STREAM, ip_protocol);
	if (listen_sock < 0) {
	    ESP_LOGE(TAG, "Unable to create socket: errno %d", errno);
	}
	ESP_LOGI(TAG, "Socket created");

	int err = bind(listen_sock, (struct sockaddr *)&destAddr, sizeof(destAddr));
	if (err != 0) {
	    ESP_LOGE(TAG, "Socket unable to bind: errno %d", errno);
	}
	ESP_LOGI(TAG, "Socket binded");

	err = listen(listen_sock, 1);
	if (err != 0) {
		ESP_LOGE(TAG, "Error occured during listen: errno %d", errno);
	}
	ESP_LOGI(TAG, "Socket listening");

    while(1){

	    struct sockaddr_in sourceAddr;
	    uint addrLen = sizeof(sourceAddr);
	    int sock = accept(listen_sock, (struct sockaddr *)&sourceAddr, &addrLen);
	    if (sock < 0) {
	        ESP_LOGE(TAG, "Unable to accept connection: errno %d", errno);
	        break;
	    }
	    ESP_LOGI(TAG, "Socket accepted");


		//Connection established, welcome banner
		const char welcome_banner[] = "LAMPARA V2.0\n";
	    send(sock,&welcome_banner,sizeof(welcome_banner),0);

		//handle received data
		while(1){
			int len = recv(sock, rx_buffer, sizeof(rx_buffer) - 1, 0);
			// Error occured during receiving
			if (len < 0) {
				ESP_LOGE(TAG, "recv failed: errno %d", errno);
				break;
			}
			// Connection closed
			else if (len == 0) {
				ESP_LOGI(TAG, "Connection closed");
				break;
			}
			
			// Data received
			else {
				inet_ntoa_r(((struct sockaddr_in *)&sourceAddr)->sin_addr.s_addr, addr_str, sizeof(addr_str) - 1);

				rx_buffer[len] = 0;
				
				// fill handler
				/*if(memcmp(rx_buffer,"fill .",5) == 0){
					int r,g,b;
					int readed = sscanf(rx_buffer,"fill %i,%i,%i",&r,&g,&b);
					if(readed == 3){
						
						if(fifo_is_full(fotogram_buffer)){
							send(sock,"BUFFER FULL\n",12,0);		
						}else{
						
							send(sock,"OK\n",4,0);
						
							for(int i = 0;i < LEDS_NUM;i++){
								leds[i*3] = (uint8_t) g&0xff;
								leds[i*3+1] = (uint8_t) r&0xff;
								leds[i*3+2] = (uint8_t) b&0xff;
							}
						
							fifo_add(fotogram_buffer,leds);
						}
						
					}else{
						send(sock,"BAD VALUES\n",12,0);
					}
				} else if(memcmp(rx_buffer,"single .",7)==0) {
					int r,g,b,pos;
					int readed = sscanf(rx_buffer,"single %i %i,%i,%i",&pos,&r,&g,&b);
					
					if(readed == 4){
						if(pos > LEDS_NUM || pos < 1){
							send(sock,"BAD INDEX\n",10,0);
						}else{
							send(sock,"OK\n",4,0);
							
							pos--;
							leds[pos*3] = (uint8_t) g&0xff;
							leds[pos*3+1] = (uint8_t) r&0xff;
							leds[pos*3+2] = (uint8_t) b&0xff;
							
							fifo_add(fotogram_buffer,leds);
						}
					}else{
						send(sock,"BAD VALUES\n",12,0);
					}
				} else if(memcmp(rx_buffer,"multi\n",6)==0){
					int r,g,b,pos = 0;
					
					int rx_buffer_index = 6;
					
					int colors_buf_index = 0;
					char colors_buf[40];
					
					int sigue = 1;
					while(sigue == 1){
						
						if(rx_buffer_index > len-1){
							len = recv(sock, rx_buffer, sizeof(rx_buffer)-1,0);
							rx_buffer_index = 0;
						}
						
						if(rx_buffer[rx_buffer_index] == '\n'){
							colors_buf[colors_buf_index] = 0;
							
							if(sscanf(colors_buf,"%i,%i,%i",&r,&g,&b) == 3){
									if(pos < LEDS_NUM-1){
										leds[pos*3] = (uint8_t) g&0xff;
										leds[pos*3+1] = (uint8_t) r&0xff;
										leds[pos*3+2] = (uint8_t) b&0xff;
										pos++;
									}else{
										send(sock,"NO MORE LEDS\n",13,0);
									}
							} else if(memcmp(colors_buf,"end",3) == 0){
								while(!fifo_is_full(fotogram_buffer))
									fifo_add(fotogram_buffer,leds);
									
								send(sock,"OK\n",3,0);
								sigue=0;
							} else {
								send(sock,"BAD VALUES\n",11,0);
							}
							
							rx_buffer_index++;
							colors_buf_index = 0;
						}else{
							colors_buf[colors_buf_index++] = rx_buffer[rx_buffer_index++];
						}
					}

				} else if(memcmp(rx_buffer,"multi_pos\n",10)==0){
					int r,g,b,pos = 0;
					
					int rx_buffer_index = 10;
					
					int colors_buf_index = 0;
					char colors_buf[40];
					
					int sigue = 1;
					while(sigue == 1){
						
						if(rx_buffer_index > len-1){
							len = recv(sock, rx_buffer, sizeof(rx_buffer)-1,0);
							rx_buffer_index = 0;
						}
						
						if(rx_buffer[rx_buffer_index] == '\n'){
							colors_buf[colors_buf_index] = 0;
							
							if(sscanf(colors_buf,"%i %i,%i,%i",&pos,&r,&g,&b) == 4){
									if(pos <= LEDS_NUM && pos >= 1){
										pos--;
										leds[pos*3] = (uint8_t) g&0xff;
										leds[pos*3+1] = (uint8_t) r&0xff;
										leds[pos*3+2] = (uint8_t) b&0xff;
									}else{
										send(sock,"BAD INDEX\n",10,0);
									}
							} else if(memcmp(colors_buf,"end",3) == 0){
								fill_ws2812(leds);
								send(sock,"OK\n",3,0);
								sigue=0;
							} else {
								send(sock,"BAD VALUES\n",11,0);
							}
							
							rx_buffer_index++;
							colors_buf_index = 0;
						}else{
							colors_buf[colors_buf_index++] = rx_buffer[rx_buffer_index++];
						}
					}
				} else if(memcmp(rx_buffer,"close\n",6)==0){
					send(sock,"OK\n",4,0);
					close(sock);
					break;
				}else{
					send(sock,"UNKNOWN\n",9,0);
				}*/
				
				if(memcmp(rx_buffer,FILL_CMD,sizeof(FILL_CMD)-2) == 0){
					int r,g,b;
					int readed = sscanf(rx_buffer,"fill %u,%u,%u",&r,&g,&b);
					if(readed == 3){
						_SEND_OK(sock)
						
						for(int i = 0;i < LEDS_NUM;i++){
							pixels_buff.data[i].r = r;
							pixels_buff.data[i].g = g;
							pixels_buff.data[i].b = b;
						}
						push_fotogram(&pixels_buff);
					}else{
						_SEND_BAD_VALUES(sock)
					}
				}else if(memcmp(rx_buffer,MULTI_CMD,sizeof(MULTI_CMD))==0){
					int sigue = 1;
					
					char aux_buffer[40];
					int aux_buffer_index = 0;
					
					int readed,r,g,b;
					int pos = 0;
					len = 6;
					
					while(sigue){
						//Para comprobar si hemos acabado el buffer
						if(rx_buffer[len] == 0){
							//Se ha acabado
							len = recv(sock,
										rx_buffer, 
										sizeof(rx_buffer) - 1,
										0);
							if(len <= 0) break;
							rx_buffer[len]=0;
							len=0;
						}
						
						
						
						//Diferentes casos:
						//   - recibimos un salto de linea: procesar la linea que nos ha llegado
						//   - otro caso, seguir almacenando en el bufer auxiliar
						
						if(rx_buffer[len] == '\n'){
							aux_buffer[aux_buffer_index] = 0;
							//Dos casos:
							//   - color
							//   - mensaje "end"
							
							if(memcmp(&aux_buffer[0],"end",3) == 0){
								//Cargar fotograma
								sigue=0;
								_SEND_OK(sock)
								push_fotogram(&pixels_buff);
							}else if((readed = sscanf(&aux_buffer[0],
														"%u,%u,%u",
														&r,
														&g,
														&b)) == 3){
								//Cargar pixel,incrementar posicion
								if(pos >= LEDS_NUM)
									_SEND_BAD_VALUES(sock)
								else {
								
									pixels_buff.data[pos].r = r;
									pixels_buff.data[pos].g = g;
									pixels_buff.data[pos].b = b;
								
									pos++;
								}
								
							}else {
								_SEND_BAD_VALUES(sock)
							}
							
							aux_buffer_index = 0;
							len++;
						}else{
							aux_buffer[aux_buffer_index++] = rx_buffer[len++];
						}
					}
					
					
				}else if(memcmp(rx_buffer,CLOSE_CMD,sizeof(CLOSE_CMD))==0){
					_SEND_OK(sock)
					close(sock);
					break;
				}else{
					const char unknown_msg[] = "UNKNOWN\n";
					send(sock,&unknown_msg,sizeof(unknown_msg),0);
				}
			}
		}

	}

    vTaskDelete(NULL);
}
