#ifndef _LEDS_H_
#define _LEDS_H_

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "driver/i2s.h"
#include "esp8266/i2s_register.h"
#include "esp8266/slc_register.h"
#include "esp8266/rom_functions.h"

#include "esp8266/pin_mux_register.h"

#define LEDS_NUM 144


// Define them here if we can't find them.
#ifndef i2c_bbpll
#define i2c_bbpll                               0x67
#define i2c_bbpll_en_audio_clock_out            4
#define i2c_bbpll_en_audio_clock_out_msb        7
#define i2c_bbpll_en_audio_clock_out_lsb        7
#define i2c_bbpll_hostid                        4

/* ROM functions which read/write internal control bus */
uint8_t rom_i2c_readReg(uint8_t block, uint8_t host_id, uint8_t reg_add);
uint8_t rom_i2c_readReg_Mask(uint8_t block, uint8_t host_id, uint8_t reg_add, uint8_t msb, uint8_t lsb);
void rom_i2c_writeReg(uint8_t block, uint8_t host_id, uint8_t reg_add, uint8_t data);
void rom_i2c_writeReg_Mask(uint8_t block, uint8_t host_id, uint8_t reg_add, uint8_t msb, uint8_t lsb, uint8_t data);

#define i2c_writeReg_Mask(block, host_id, reg_add, Msb, Lsb, indata)  rom_i2c_writeReg_Mask(block, host_id, reg_add, Msb, Lsb, indata)
#define i2c_readReg_Mask(block, host_id, reg_add, Msb, Lsb)  rom_i2c_readReg_Mask(block, host_id, reg_add, Msb, Lsb)
#define i2c_writeReg_Mask_def(block, reg_add, indata) \
    i2c_writeReg_Mask(block, block##_hostid,  reg_add,  reg_add##_msb,  reg_add##_lsb,  indata)
#define i2c_readReg_Mask_def(block, reg_add) \
    i2c_readReg_Mask(block, block##_hostid,  reg_add,  reg_add##_msb,  reg_add##_lsb)
#endif

typedef struct lldesc {
    uint32_t                blocksize : 12;
    uint32_t                datalen   : 12;
    uint32_t                unused    :  5;
    uint32_t                sub_sof   :  1;
    uint32_t                eof       :  1;
    volatile uint32_t       owner     :  1; // DMA can change this value
    uint32_t               *buf_ptr;
    struct lldesc          *next_link_ptr;
} lldesc_t;

struct pixel {
	uint8_t r,g,b;
};
typedef struct pixel pixel;

struct fotogram_s {
	pixel data[LEDS_NUM];
};
typedef struct fotogram_s fotogram;


lldesc_t i2sBufDescOut;
lldesc_t i2sBufDescZeroes;

#define I2S_BYTES_PER_BYTE 4
#define _INIT_LEDS  init_leds(); \
					TimerHandle_t t = xTimerCreate("leds_timer",pdMS_TO_TICKS( 25 ), pdTRUE,0,update_fotogram); \
					xTimerStart(t,0);

static uint32_t i2sBlock[LEDS_NUM*sizeof(pixel)];
static uint32_t i2sZeroes[40];

static const uint16_t conversion[] = {
	0b1000100010001000,
	0b1000100010001110,
	0b1000100011101000,
	0b1000100011101110,

	0b1000111010001000,
	0b1000111010001110,
	0b1000111011101000,
	0b1000111011101110,

	0b1110100010001000,
	0b1110100010001110,
	0b1110100011101000,
	0b1110100011101110,

	0b1110111010001000,
	0b1110111010001110,
	0b1110111011101000,
	0b1110111011101110,
};

void push_fotogram(fotogram *f);
void update_fotogram(TimerHandle_t xTimer);
void init_leds();
void fill_leds(pixel *p);

#endif _LEDS_H_
