#include "leds.h"
#include "fifo.h"

extern fifo_t fotogram_fifo_t;

pixel alert;
fotogram last;
void update_fotogram(TimerHandle_t xTimer){
	if(!fifo_is_empty(&fotogram_fifo_t)) {
		fifo_get(&fotogram_fifo_t, &last);
		alert.r = 0;
	}else{
		alert.r+=4;
		last.data[4]=alert;
	}
	fill_leds(&(last.data));
}


void push_fotogram(fotogram *f){
	while(fifo_is_full(&fotogram_fifo_t));
	
	fifo_add(&fotogram_fifo_t,f);
}

void init_leds(){
    memset(&i2sZeroes[0],0,160);
    
    SET_PERI_REG_MASK(SLC_CONF0, SLC_RXLINK_RST);//|SLC_TXLINK_RST);
	CLEAR_PERI_REG_MASK(SLC_CONF0, SLC_RXLINK_RST);//|SLC_TXLINK_RST);

	//Clear DMA int flags
	SET_PERI_REG_MASK(SLC_INT_CLR,  0xffffffff);
	CLEAR_PERI_REG_MASK(SLC_INT_CLR,  0xffffffff);

	//Enable and configure DMA
	CLEAR_PERI_REG_MASK(SLC_CONF0, (SLC_MODE<<SLC_MODE_S));
	SET_PERI_REG_MASK(SLC_CONF0,(1<<SLC_MODE_S));
	SET_PERI_REG_MASK(SLC_RX_DSCR_CONF,SLC_INFOR_NO_REPLACE|SLC_TOKEN_NO_REPLACE);
    CLEAR_PERI_REG_MASK(SLC_RX_DSCR_CONF, SLC_RX_FILL_EN|SLC_RX_EOF_MODE | SLC_RX_FILL_MODE);

    i2sBufDescOut.owner = 1;
	i2sBufDescOut.eof = 1;
	i2sBufDescOut.sub_sof = 0;
	i2sBufDescOut.datalen = LEDS_NUM*sizeof(pixel)*I2S_BYTES_PER_BYTE;  //Size (in bytes)
	i2sBufDescOut.blocksize = LEDS_NUM*sizeof(pixel)*I2S_BYTES_PER_BYTE; //Size (in bytes)
	i2sBufDescOut.buf_ptr=(uint32_t)&i2sBlock[0];
	i2sBufDescOut.unused=0;
	i2sBufDescOut.next_link_ptr=&i2sBufDescZeroes; //At the end, just redirect the DMA to the zero buffer.

	i2sBufDescZeroes.owner = 1;
	i2sBufDescZeroes.eof = 1;
	i2sBufDescZeroes.sub_sof = 0;
	i2sBufDescZeroes.datalen = 160;
	i2sBufDescZeroes.blocksize = 160;
	i2sBufDescZeroes.buf_ptr=(uint32_t)&i2sZeroes[0];
	i2sBufDescZeroes.unused=0;
	i2sBufDescZeroes.next_link_ptr=&i2sBufDescOut;
    
  	CLEAR_PERI_REG_MASK(SLC_RX_LINK,SLC_RXLINK_DESCADDR_MASK);
	SET_PERI_REG_MASK(SLC_RX_LINK, ((uint32_t)&i2sBufDescOut) & SLC_RXLINK_DESCADDR_MASK);
    
    SET_PERI_REG_MASK(SLC_RX_LINK, SLC_RXLINK_START);

    PIN_FUNC_SELECT(PERIPHS_IO_MUX_U0RXD_U, FUNC_I2SO_DATA);

	//Enable clock to i2s subsystem
	i2c_writeReg_Mask_def(i2c_bbpll, i2c_bbpll_en_audio_clock_out, 1);

	//Reset I2S subsystem
	CLEAR_PERI_REG_MASK(I2SCONF,I2S_I2S_RESET_MASK);
	SET_PERI_REG_MASK(I2SCONF,I2S_I2S_RESET_MASK);
	CLEAR_PERI_REG_MASK(I2SCONF,I2S_I2S_RESET_MASK);

	//Select 16bits per channel (FIFO_MOD=0), no DMA access (FIFO only)
	CLEAR_PERI_REG_MASK(I2S_FIFO_CONF, I2S_I2S_DSCR_EN|(I2S_I2S_RX_FIFO_MOD<<I2S_I2S_RX_FIFO_MOD_S)|(I2S_I2S_TX_FIFO_MOD<<I2S_I2S_TX_FIFO_MOD_S));
	//Enable DMA in i2s subsystem
	SET_PERI_REG_MASK(I2S_FIFO_CONF, I2S_I2S_DSCR_EN);

    CLEAR_PERI_REG_MASK(I2SCONF, I2S_TRANS_SLAVE_MOD|
						(I2S_BITS_MOD<<I2S_BITS_MOD_S)|
						(I2S_BCK_DIV_NUM <<I2S_BCK_DIV_NUM_S)|
						(I2S_CLKM_DIV_NUM<<I2S_CLKM_DIV_NUM_S));
	SET_PERI_REG_MASK(I2SCONF, I2S_RIGHT_FIRST|I2S_MSB_RIGHT|I2S_RECE_SLAVE_MOD|
						I2S_RECE_MSB_SHIFT|I2S_TRANS_MSB_SHIFT|
						(((18)&I2S_BCK_DIV_NUM )<<I2S_BCK_DIV_NUM_S)|
						(((3)&I2S_CLKM_DIV_NUM)<<I2S_CLKM_DIV_NUM_S));


	//Start transmission
	SET_PERI_REG_MASK(I2SCONF,I2S_I2S_TX_START);
}

void fill_leds(pixel *p){

    for(int i = 0;i < LEDS_NUM;i++){
        unsigned short lnibble = conversion[(p[i].g)&0x0f];
        unsigned short hnibble = conversion[((p[i].g)>>4)&0x0f];
        i2sBlock[3*i] = (hnibble<<16 & 0xffff0000) | (lnibble & 0x0000ffff);
        
        lnibble = conversion[(p[i].r)&0x0f];
        hnibble = conversion[((p[i].r)>>4)&0x0f];
        i2sBlock[3*i+1] = (hnibble<<16 & 0xffff0000) | (lnibble & 0x0000ffff);
        
        lnibble = conversion[(p[i].b)&0x0f];
        hnibble = conversion[((p[i].b)>>4)&0x0f];
        i2sBlock[3*i+2] = (hnibble<<16 & 0xffff0000) | (lnibble & 0x0000ffff);
    }

    i2sBufDescOut.owner = 1;
    i2sBufDescOut.eof = 1;
    i2sBufDescOut.sub_sof = 0;
    i2sBufDescOut.datalen = LEDS_NUM*sizeof(pixel)*I2S_BYTES_PER_BYTE;  //Size (in bytes)
    i2sBufDescOut.blocksize = LEDS_NUM*sizeof(pixel)*I2S_BYTES_PER_BYTE; //Size (in bytes)
    i2sBufDescOut.buf_ptr = (uint32_t)&i2sBlock[0];
    i2sBufDescOut.unused = 0;
    i2sBufDescOut.next_link_ptr=&i2sBufDescZeroes;

    SET_PERI_REG_MASK(SLC_RX_LINK, SLC_RXLINK_STOP);
   	CLEAR_PERI_REG_MASK(SLC_RX_LINK,SLC_RXLINK_DESCADDR_MASK);
    SET_PERI_REG_MASK(SLC_RX_LINK, ((uint32_t)&i2sBufDescOut) & SLC_RXLINK_DESCADDR_MASK);
    SET_PERI_REG_MASK(SLC_RX_LINK, SLC_RXLINK_START);
}
