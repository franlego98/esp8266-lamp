#ifndef _NETWORKING_H_
#define _NETWORKING_H_

#define PORT    7000

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>

#include "esp_log.h"

extern const char *TAG;

#define _START_SERVER_TASK xTaskCreate(server_task, "server_task", 4096, NULL, 5, NULL);

#define CLOSE_CMD "close\n"
#define FILL_CMD "fill ."
#define MULTI_CMD "multi\n"

#define _SEND_OK(s) send(s,"OK\n",3,0);
#define _SEND_BAD_VALUES(s) send(s,"BAD VALUES\n",12,0);

void server_task(void *pvParameters);


#endif
