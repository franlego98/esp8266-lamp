#include "wifi.h"
#include "networking.h"
#include "fifo.h"
#include "leds.h"
#include "esp_log.h"

#define FOTOGRAMS_BUFFER_LEN 80

const char *TAG = "Lampara";

fotogram pixels_buff;
fotogram fotogram_buffer[FOTOGRAMS_BUFFER_LEN];
struct fifo_descriptor fotogram_fifo_t;


void app_main(){
	fifo_t ok = fifo_create_static(&fotogram_fifo_t,&fotogram_buffer[0], FOTOGRAMS_BUFFER_LEN, sizeof(fotogram));
	
	_INIT_LEDS
	
	_INIT_WIFI
	
	_START_SERVER_TASK
}
