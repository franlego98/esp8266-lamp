#ifndef _WIFI_H_
#define _WIFI_H_

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_event_loop.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_log.h"

#define WIFI_SSID "ConocimientoEnElAire"
#define WIFI_PASS "noeslomismotresbolasnegrasquetresnegrasenbolas"

#define _INIT_WIFI initialise_wifi(); wait_for_ip();

EventGroupHandle_t wifi_event_group;
#define IPV4_GOTIP_BIT BIT0
extern const char *TAG;

void wait_for_ip();
void initialise_wifi(void);
esp_err_t event_handler(void *ctx, system_event_t *event);

#endif
